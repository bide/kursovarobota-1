<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

    public const SORT_TYPES = [
        '1' => 'за назвою пісні',
        '2' => 'за місцем',
    ];

    protected $table = 'members';
    public $timestamps = false;
    public $incrementing = true;

    protected $fillable = [
        'fio',
        'song',
        'raiting',
    ];

    public function getMembers($request){
        $query = Member::query();

        if(filled($request->song))
            $query->where('song', 'LIKE', '%'.$request->song.'%');

        if(filled($request->top3)){
            $query->where('rating', '=', '1')->limit(3);
        }

        switch ($request->sort){
            case '1':
                $query->orderBy('fio');
                break;
            case '2':
                $query->orderBy('rating');
                break;
            default:
                break;
        }

        return $query->get();
    }
}
