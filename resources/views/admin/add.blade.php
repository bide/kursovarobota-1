@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-5">
        <h1>Додати учасника</h1>

        <form action="/members" method="post">
            @csrf
            <span>Псевдоним: <input type="text" name="fio" class="form-control" placeholder="Псевдоним"></span> <br><br>
            <span>Назва пісні: <input type="text" name="song" class="form-control" placeholder="Назва пісні"></span> <br><br>
            <span>Отримане місце: <input type="number" name="rating" class="form-control" placeholder="Отримане місце"></span> <br><br>
            <input type="submit" class="form-control btn btn-success" value="Додати">
        </form>
    </div>
</div>
@endsection
