@extends('layouts.app')

@section('content')
    <h1 align="center">Інформаційна система Євробачення</h1>
    <table align="center" class="table table-bordered table-hover">
        <tr>
            <td>ID</td>
            <td>ФІО виконавця</td>
            <td>Назва пісні</td>
            <td>Рейтинг</td>
        </tr>
        @foreach($members as $member)
            <tr>
                <td>{{$member->id}}</td>
                <td><a href="/members/{{$member->id}}">{{$member->fio}}</a></td>
                <td>{{$member->song}}</td>
                <td>{{$member->rating}}</td>
            </tr>
        @endforeach
    </table>

    <form align="center">
        <a href="/members">Адмінка</a><br>
        <a href="/login">Увійти</a><br>
        <a href="/register">Зареєструватись</a>
    </form>
@endsection
