<?php

namespace Tests\Feature;

use App\Models\Member;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MemberTest extends TestCase
{
    private $member;

    public function setUp(): void
    {
        parent::setUp();

        $this->member = new Member();
        $this->member->fio = 'Maneskin';
        $this->member->song = 'ZITTI E BUONI';
        $this->member->rating = 1;
        $this->member->save();
    }

    public function test_Status()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

	public function test_Status()
    {
        $response = $this->get('/');
        $response->assertStatus(302);
    }

    public function test_View()
    {
        $view = $this->view('unit', ['members' => [$this->member]]);
        $view->assertSeeText('Maneskin');
    }

    public function test_ViewWithMock()
    {
        $stub = $this->getMockBuilder(Member::class)->getMock();

        $stub->method('getMembers')
            ->willReturn([
                (object)['id' => 1, 'fio' => 'test1', 'song' => 'test1', 'rating' => 1],
                (object)['id' => 2, 'fio' => 'test2', 'song' => 'test1', 'rating' => 2]
            ]);
        $view = $this->view('unit', ['members' => $stub->getMembers(null)]);
        $view->assertSee('test1');
    }

    public function test_SeeInDB()
    {
        $member = Member::query()->where('fio', '=', 'Maneskin')->first();

        $this->assertEquals('Maneskin', $member->fio);
        $this->assertEquals(1, $member->rating);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->member->delete();
    }


}
