@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-5">
        <h1>Редагувати учасника</h1>

        <form action="/members/{{$member->id}}" method="post">
            @csrf
            @method('PUT')
            <span>Псевдоним: <input type="text" name="fio" class="form-control" placeholder="Псевдоним" value="{{$member->fio}}"></span> <br><br>
            <span>Назва пісні: <input type="text" name="song" class="form-control" placeholder="Назва пісні" value="{{$member->song}}"></span> <br><br>
            <span>Отримане місце: <input type="text" name="rating" class="form-control" placeholder="отримане місце" value="{{$member->rating}}"></span> <br><br>
            <input type="submit" class="btn btn-success form-control" value="Редагувати">
        </form>
        </div>
    </div>
@endsection


