DROP DATABASE IF EXISTS eurovision;
CREATE DATABASE eurovision;
USE eurovision;

CREATE TABLE members(
	id INT PRIMARY KEY AUTO_INCREMENT,
    fio VARCHAR(30),
    song VARCHAR(30),
    rating INT
);

INSERT INTO members(fio, song, rating) VALUES 
('Konchita Wurst','Rise Like a Phoenix ',1),
('Alexander Rybak','Fairy Tales',1),
('Dima Bilan','Believe',1),
('Papich','Believe',2),
('Ruslana','Wild dances',2);
