@extends('layouts.app')

@section('content')
    <h1 align="center">Адмінка</h1>
    <form align="center">
        <span>Пошук за назвою пісні: <input type="text" name="song" placeholder="Назва"></span>

        <span>Сортувати:
            <select name="sort">
                <option value="">Не сортувати</option>
                @foreach(\App\Models\member::SORT_TYPES as $key => $type)
                    <option value="{{$key}}" {{($request->sort == $key) ? 'selected' : ''}}>{{$type}}</option>
                @endforeach
            </select>
        </span>

        <label for="top">Топ 3 переможців: </label>
        <input type="checkbox" id="top" name="top3" {{($request->top3 == 'on') ? 'checked' : ''}}>
        <input type="submit" value="Відобразити">
    </form>
    <form align="center">
        <a href="/members">Скинути фільтри</a><br><br>
    </form>

    <table align="center" class="table table-bordered table-hover" style="max-width: 1000px">
        <tr>
            <td>ID</td>
            <td>ФІО виконавця</td>
            <td>Назва пісні</td>
            <td>Рейтинг</td>
            <td colspan="2" class="text-center">Керування</td>
        </tr>
        @foreach($members as $member)
            <tr>
                <td>{{$member->id}}</td>
                <td><a href="/members/{{$member->id}}">{{$member->fio}}</a></td>
                <td>{{$member->song}}</td>
                <td>{{$member->rating}}</td>
                <td><a href="/members/{{$member->id}}/edit" class="btn form-control btn-warning">Редагувати запис</a></td>
                <form action="/members/{{$member->id}}" method="post">
                    @csrf
                    @method('DELETE')
                    <td><input type="submit" class="btn form-control btn-danger" value="Видалити місце"></td>
                </form>
            </tr>
        @endforeach
    </table>

    <form align="center">
        <a href="/">На головну</a>
        <a href="/members/create">Додати учасника</a>
    </form>

@endsection
