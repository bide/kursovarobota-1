<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MemberController;

Route::get('/', [MemberController::class, 'main']);

Route::resource('members', MemberController::class);
Auth::routes();

