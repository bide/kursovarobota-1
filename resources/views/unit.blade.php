<table border="1">
    <tr>
        <td>Учасник</td>
        <td>Пісня</td>
        <td>Рейтинг</td>
    </tr>
    @foreach($members as $member)
        <tr>
            <td>{{$member->fio}}</td>
            <td>{{$member->song}}</td>
            <td>{{$member->rating}}</td>
        </tr>
    @endforeach
</table>
